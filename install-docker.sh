#!/usr/bin/env bash

#############################################################
#                   EDIT THIS FOR DIFFERENT OS              #
#############################################################
 CODENAME=$(lsb_release -u -cs) # for Elementary OS
 #CODENAME=$(lsb_release -cs) # For Ubuntu
#___________________________________________________________#

# Remove old installs
sudo apt-get remove docker docker-engine docker.io docker-ce -y

# Update package index
sudo apt-get update

# Install packages to allow apt to use a repo over HTTPS
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common -y

# Add Docker official GPG key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

# Setup repository
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $CODENAME \
   stable" -y

# Install Docker CE
sudo apt-get update -y
sudo apt-get install docker-ce -y

# Cleanup
sudo apt-get autoremove -y

# Create new group and add user
sudo groupadd docker
sudo -E usermod -aG docker $USER
